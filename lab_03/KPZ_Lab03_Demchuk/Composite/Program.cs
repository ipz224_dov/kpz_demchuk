﻿using Composite.CompositeClasses;
using Composite.lab05.Command;
using Composite.lab05.State.States;
using Composite.lab05.Visitor;

var title = new LightElementNode(
    new LightElementInfo(
        tagName: "h1",
        isBlockLevel: true,
        isSelfClosing: false,
        cssClasses: new List<string>()
    ),
    new List<LightNode> { new LightTextNode("New Marvel Movie Announcement") }
);


var releaseDate = new LightElementNode(
    new LightElementInfo(
        tagName: "p",
        isBlockLevel: false,
        isSelfClosing: false,
        cssClasses: new List<string> { "release-date" }
    ),
    new List<LightNode> { new LightTextNode("Release date: October 2024") }
);

var description = new LightElementNode(
    new LightElementInfo(
        tagName: "p",
        isBlockLevel: false,
        isSelfClosing: false,
        cssClasses: new List<string> { "description" }
    ),
    new List<LightNode> { new LightTextNode("This movie promises to be an epic adventure!") }
);

var cast = new LightElementNode(
    new LightElementInfo(
        tagName: "ul",
        isBlockLevel: false,
        isSelfClosing: false,
        cssClasses: new List<string> { "cast" }
    ),
    new List<LightNode> {
        new LightElementNode(
            new LightElementInfo(
                tagName: "li",
                isBlockLevel: false,
                isSelfClosing: false,
                cssClasses: new List<string>()
            ),
            new List<LightNode> { new LightTextNode("Tom Holland") }
        ),
        new LightElementNode(
            new LightElementInfo(
                tagName: "li",
                isBlockLevel: false,
                isSelfClosing: false,
                cssClasses: new List<string>()
            ),
            new List<LightNode> { new LightTextNode("Zendaya") }
        ),
        new LightElementNode(
            new LightElementInfo(
                tagName: "li",
                isBlockLevel: false,
                isSelfClosing: false,
                cssClasses: new List<string>()
            ),
            new List<LightNode> { new LightTextNode("Benedict Cumberbatch") }
        )
    }
);

var filmDetails = new LightElementNode(
    new LightElementInfo(
        tagName: "div",
        isBlockLevel: true,
        isSelfClosing: false,
        cssClasses: new List<string> { "film-details" }
    ),
    new List<LightNode> { title, releaseDate, description, cast }
);

#region Iterator
/*var breadthIterator = filmDetails.CreateBreadthFirstIterator();

Console.ForegroundColor = ConsoleColor.Red;
Console.WriteLine("Breadth First Iterator:\n");
Console.ResetColor();

while (breadthIterator.HasNext())
{
    Console.WriteLine("----------------------------------------------------------------------------");
    Console.ForegroundColor = ConsoleColor.Cyan;
    Console.WriteLine(breadthIterator.GetNext().OuterHtml);
    Console.ResetColor();
    Console.WriteLine("----------------------------------------------------------------------------\n");
}

var depthIterator = filmDetails.CreateDepthFirstIterator();

Console.ForegroundColor = ConsoleColor.Green;
Console.WriteLine("Depth First Iterator:\n");
Console.ResetColor();

while (depthIterator.HasNext())
{
    Console.WriteLine("----------------------------------------------------------------------------");
    Console.ForegroundColor = ConsoleColor.Red;
    Console.WriteLine(depthIterator.GetNext().OuterHtml);
    Console.ResetColor();
    Console.WriteLine("----------------------------------------------------------------------------\n");
}*/
#endregion

#region Command
/*var clickElementCommand = new ClickElementCommand(filmDetails);
var removeElementCommand = new RemoveElementCommand(filmDetails, cast);
var addElementCommand = new AddElementCommand(filmDetails, cast);

Console.ForegroundColor = ConsoleColor.Yellow;
Console.WriteLine("----------------------------------------");
Console.WriteLine("Click Element Command:");
clickElementCommand.Execute();
Console.WriteLine("----------------------------------------");
Console.ResetColor();

Console.ForegroundColor = ConsoleColor.Blue;
Console.WriteLine("----------------------------------------");
Console.WriteLine("Remove Element Command:");
removeElementCommand.Execute();
Console.WriteLine("----------------------------------------");
Console.ResetColor();

Console.ForegroundColor = ConsoleColor.Red;
Console.WriteLine("----------------------------------------");
Console.WriteLine("Add Element Command:");
addElementCommand.Execute();
Console.WriteLine("----------------------------------------");
Console.ResetColor();*/
#endregion

#region State
/*
filmDetails.SetState(new HiddenState());
filmDetails.HandleState();

filmDetails.SetState(new VisibleState());
filmDetails.HandleState();
*/
#endregion

#region Visitor
/*
var cssClassesCounterVisitor = new CssClassesCounterVisitor();
cssClassesCounterVisitor.Visit(filmDetails);

Console.WriteLine();
var addWatermarkVisitor = new AddWatermarkVisitor();
addWatermarkVisitor.Visit(filmDetails);
*/
#endregion

#region Template Method


#endregion