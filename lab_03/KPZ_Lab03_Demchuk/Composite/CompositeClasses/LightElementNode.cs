using System.Text;
using Composite.lab05.Iterator;
using Composite.lab05.State;
using Composite.lab05.Visitor;

namespace Composite.CompositeClasses
{
    public class LightElementNode(LightElementInfo elementInfo, List<LightNode> children) : LightNode, IIterableCollection, IComponent
    {
        public LightElementInfo ElementInfo => elementInfo;
        private State _elementState = null!;
        public List<LightNode> Children { get; } = children;

        public override string InnerHtml
        {
            get
            {
                var sb = new StringBuilder();

                foreach (var child in Children)
                {
                    sb.Append(child.OuterHtml);
                }

                return sb.ToString();
            }
        }

        public override string OuterHtml
        {
            get
            {
                var sb = new StringBuilder();
                if (elementInfo.IsBlockLevel)
                {
                    sb.Append("\n");
                }
                sb.Append($"<{elementInfo.TagName}");

                foreach (var cssClass in elementInfo.CssClasses)
                {
                    sb.Append($" class=\"{cssClass}\"");
                }

                sb.Append(">");

                sb.Append(InnerHtml);
                
                switch (elementInfo.IsSelfClosing)
                {
                    case true:
                        sb.Append("/>");
                        break;
                    case false:
                        sb.Append($"</{elementInfo.TagName}>");
                        sb.AppendLine();
                        break;
                }

                return sb.ToString();
            }
        }

        public IIterator CreateDepthFirstIterator()
        {
            return new DepthFirstIterator(this);
        }

        public IIterator CreateBreadthFirstIterator()
        {
            return new BreadthFirstIterator(this);
        }
        
        public void SetState(State elementState)
        {
            _elementState = elementState;
        }
        public void HandleState()
        {
            _elementState.HandleState(this);
        }

        public void Visit(LightElementNode element)
        {
            throw new NotImplementedException();
        }

        public void Accept(IVisitor visitor)
        {
            visitor.Visit(this);
        }
    }
}