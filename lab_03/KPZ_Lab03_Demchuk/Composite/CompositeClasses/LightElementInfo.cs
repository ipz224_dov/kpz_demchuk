namespace Composite.CompositeClasses;

public class LightElementInfo(string tagName, bool isBlockLevel, bool isSelfClosing, List<string> cssClasses)
{
    public string TagName { get; } = tagName;
    public bool IsBlockLevel { get; } = isBlockLevel;
    public bool IsSelfClosing { get; } = isSelfClosing;
    public List<string> CssClasses { get; } = cssClasses;
}