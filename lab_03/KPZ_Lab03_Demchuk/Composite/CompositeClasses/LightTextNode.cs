namespace Composite.CompositeClasses;

public class LightTextNode(string text) : LightNode
{
    public override string InnerHtml { get; } = text;
    public override string OuterHtml { get; } = text;
}