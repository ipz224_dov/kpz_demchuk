using Composite.CompositeClasses;
using Composite.lab05.Command.Interface;

namespace Composite.lab05.Command;

public class AddElementCommand(LightElementNode parentElementNode, LightNode element): ICommand
{
    public void Execute()
    {
        parentElementNode.Children.Add(element);
        Console.WriteLine(parentElementNode.OuterHtml);
    }
}