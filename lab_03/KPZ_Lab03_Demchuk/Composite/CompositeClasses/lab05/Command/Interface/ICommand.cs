namespace Composite.lab05.Command.Interface;

public interface ICommand
{
    void Execute();
}