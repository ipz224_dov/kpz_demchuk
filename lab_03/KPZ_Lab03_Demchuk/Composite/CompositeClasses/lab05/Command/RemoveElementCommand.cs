using Composite.CompositeClasses;
using Composite.lab05.Command.Interface;

namespace Composite.lab05.Command;

public class RemoveElementCommand(LightElementNode parentElementNode, LightNode element): ICommand
{
    public void Execute()
    {
        if (parentElementNode.Children.Contains(element))
            parentElementNode.Children.Remove(element);
        
        Console.WriteLine(parentElementNode.OuterHtml);
    }
}