using Composite.CompositeClasses;
using Composite.lab05.Command.Interface;

namespace Composite.lab05.Command;

public class ClickElementCommand(LightNode element): ICommand
{
    public void Execute()
    {
        Console.ForegroundColor = ConsoleColor.Green;
        Console.WriteLine("Element clicked:");
        Console.WriteLine(element.OuterHtml);
        Console.WriteLine();
        Console.ResetColor();
    }
}