using Composite.CompositeClasses;

namespace Composite.lab05.Iterator;

public interface IIterator
{
    bool HasNext();
    LightNode GetNext();
}