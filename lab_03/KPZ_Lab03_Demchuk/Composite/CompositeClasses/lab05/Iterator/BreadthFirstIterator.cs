using Composite.CompositeClasses;

namespace Composite.lab05.Iterator;

public class BreadthFirstIterator: IIterator
{
    private readonly Queue<LightNode> _queue = new();

    public BreadthFirstIterator(LightNode root)
    {
        _queue.Enqueue(root);
    }

    public bool HasNext()
    {
        return _queue.Count > 0;
    }

    public LightNode GetNext()
    {
        if (!HasNext())
            throw new InvalidOperationException("Queue is empty");

        LightNode node = _queue.Dequeue();
        if (node is not LightElementNode elementNode) return node;
        foreach (var child in elementNode.Children)
            _queue.Enqueue(child);
        
        return node;
    }
}