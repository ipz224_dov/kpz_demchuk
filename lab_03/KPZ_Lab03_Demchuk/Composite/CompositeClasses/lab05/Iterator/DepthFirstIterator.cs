using Composite.CompositeClasses;

namespace Composite.lab05.Iterator
{
    public class DepthFirstIterator : IIterator
    {
        private readonly Stack<LightNode> _stack = new();

        public DepthFirstIterator(LightNode root)
        {
            _stack.Push(root);
        }

        public bool HasNext()
        {
            return _stack.Count > 0;
        }

        public LightNode GetNext()
        {
            if (!HasNext())
                throw new InvalidOperationException("Stack is empty");
            
            LightNode node = _stack.Pop();
            if (node is not LightElementNode elementNode) return node;
            foreach (var child in elementNode.Children)
                _stack.Push(child);

            return node;
        }
    }
}