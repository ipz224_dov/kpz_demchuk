namespace Composite.lab05.Iterator;

public interface IIterableCollection
{
    IIterator CreateDepthFirstIterator();
    IIterator CreateBreadthFirstIterator();
}