using Composite.CompositeClasses;

namespace Composite.lab05.Template_Method;

public abstract class AbstractElement
{
    public void ProcessElement(LightElementNode element)
    {
        OnCreated();
        OnInserted();
        OnRemoved();
        OnStylesApplied();
        OnClassListApplied();
        OnTextRendered();
    }

    protected void OnCreated()
    {
        Console.WriteLine("Element created");
    }

    protected void OnInserted()
    {
        Console.WriteLine("Element inserted");
    }

    protected void OnRemoved()
    {
        Console.WriteLine("Element removed");
    }

    protected void OnStylesApplied()
    {
        Console.WriteLine("Styles applied");
    }

    protected void OnClassListApplied()
    {
        Console.WriteLine("Class list applied");
    }

    protected void OnTextRendered()
    {
        Console.WriteLine("Text rendered");
    }
}