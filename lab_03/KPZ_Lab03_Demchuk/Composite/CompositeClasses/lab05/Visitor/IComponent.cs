namespace Composite.lab05.Visitor;

public interface IComponent
{
    public void Accept(IVisitor visitor); 
}