using Composite.CompositeClasses;

namespace Composite.lab05.Visitor;

public class CssClassesCounterVisitor : IVisitor
{
    public void Visit(LightElementNode element)
    {
        Console.WriteLine("--------------------------------------------------------");
        Console.WriteLine("CssClassesCount visitor:");
        Console.WriteLine("--------------------------------------------------------");
        Console.WriteLine("Number of CSS classes: " + element.ElementInfo.CssClasses.Count);
        Console.WriteLine("--------------------------------------------------------");
    }
}