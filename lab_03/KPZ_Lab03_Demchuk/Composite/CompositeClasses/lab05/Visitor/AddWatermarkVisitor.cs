using Composite.CompositeClasses;

namespace Composite.lab05.Visitor;

public class AddWatermarkVisitor : IVisitor
{
    public void Visit(LightElementNode element)
    {
        Console.WriteLine("--------------------------------------------------------");
        Console.WriteLine("Adding watermark visitor:");
        Console.WriteLine("--------------------------------------------------------");
        element.Children.Add(new LightTextNode("Olesia Watermark"));
        Console.WriteLine(element.OuterHtml);
        Console.WriteLine("--------------------------------------------------------");
    }
}