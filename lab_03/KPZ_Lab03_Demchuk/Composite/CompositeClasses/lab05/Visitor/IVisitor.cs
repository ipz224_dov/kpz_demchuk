using Composite.CompositeClasses;

namespace Composite.lab05.Visitor;

public interface IVisitor
{
    void Visit(LightElementNode element);
}