using Composite.CompositeClasses;

namespace Composite.lab05.State;

public abstract class State
{
    public abstract void HandleState(LightElementNode lightElement);
}