using Composite.CompositeClasses;

namespace Composite.lab05.State.States;

public class VisibleState: State
{
    public override void HandleState(LightElementNode lightElement)
    {
        Console.ForegroundColor = ConsoleColor.Green;
        Console.WriteLine("Visible state");
        Console.WriteLine("------------------------------------------------------------");
        Console.WriteLine(lightElement.OuterHtml);
        Console.WriteLine("------------------------------------------------------------");
    }
}