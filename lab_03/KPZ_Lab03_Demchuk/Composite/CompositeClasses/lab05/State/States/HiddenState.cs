using Composite.CompositeClasses;

namespace Composite.lab05.State.States;

public class HiddenState: State
{
    public override void HandleState(LightElementNode lightElement)
    {
        Console.ForegroundColor = ConsoleColor.Red;
        Console.WriteLine("Hidden state");
        Console.WriteLine("------------------------------------------------------------");
        Console.WriteLine(lightElement.OuterHtml);
        Console.WriteLine("------------------------------------------------------------");
    }
}