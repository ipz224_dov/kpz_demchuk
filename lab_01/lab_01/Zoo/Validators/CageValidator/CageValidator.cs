using Zoo.Interfaces;

namespace Zoo.Validators.CageValidator;

public class CageValidator: ICageValidator
{
    public bool IsAnimalTypeValid(ICage cage, IAnimal animal)
    {
        return cage.AnimalType == animal.Type;
    }

    public void CanAddAnimal(ICage cage, IAnimal animal)
    {
        if (!IsAnimalTypeValid(cage, animal)) return;
        switch (cage.Capacity > cage.Animals.Count)
        {
            case false:
                throw new Exception("Cage is full");
        }

    }

    public void CanRemoveAnimal(ICage cage, IAnimal animal)
    {
        switch (cage.Animals.Contains(animal))
        {
            case false:
                throw new Exception("Animal not found");
        }
    }
}