using Zoo.Interfaces;

namespace Zoo.Validators.CageValidator;

public interface ICageValidator
{
    bool IsAnimalTypeValid(ICage cage, IAnimal animal);
    void CanAddAnimal(ICage cage, IAnimal animal);
    void CanRemoveAnimal(ICage cage, IAnimal animal);
}