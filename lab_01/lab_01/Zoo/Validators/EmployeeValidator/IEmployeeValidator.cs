using Zoo.Interfaces;

namespace Zoo.Validators.EmployeeValidator;

public interface IEmployeeValidator
{
    void CanAddEmployee(IEmployee employee, List<IEmployee> employees);
    void CanRemoveEmployee(IEmployee employee, List<IEmployee> employees);
    void CanChangeSalary(IEmployee employee, int salary);
}