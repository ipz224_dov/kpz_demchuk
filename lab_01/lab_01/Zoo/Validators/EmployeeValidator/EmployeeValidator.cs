using Zoo.Interfaces;

namespace Zoo.Validators.EmployeeValidator;

public class EmployeeValidator: IEmployeeValidator
{
    public void CanAddEmployee(IEmployee employee, List<IEmployee> employees)
    {
        if (employees.Contains(employee))
        {
            throw new Exception("Employee already exists");
        }
    }

    public void CanRemoveEmployee(IEmployee employee, List<IEmployee> employees)
    {
        if (!employees.Contains(employee))
        {
            throw new Exception("Employee does not exist");
        }
    }

    public void CanChangeSalary(IEmployee employee, int salary)
    {
        if (salary < 0)
        {
            throw new Exception("Salary cannot be negative");
        }
    }
}