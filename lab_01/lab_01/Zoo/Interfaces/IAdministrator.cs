using Zoo.Models;

namespace Zoo.Interfaces;

public interface IAdministrator: IEmployee
{
    List<IEmployee> Employees { get; }
    void AddEmployee(IEmployee employee);
    void RemoveEmployee(IEmployee employee);
    void ChangeSalary(IEmployee employee, int salary);
}