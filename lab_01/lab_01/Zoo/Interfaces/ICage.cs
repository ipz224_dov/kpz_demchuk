namespace Zoo.Interfaces;

public interface ICage
{
    IType AnimalType { get; }
    List<IAnimal> Animals { get; }
    int Capacity { get; }
    void AddAnimal(IAnimal animal);
    void RemoveAnimal(IAnimal animal);
}