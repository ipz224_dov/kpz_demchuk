namespace Zoo.Interfaces;

public interface IInventory<T>
{
    List<T> Entities { get; set; }
    void DisplayInventory(List<T> entities);
}