namespace Zoo.Interfaces;

public interface IType
{
    string Name { get; }
    string Description { get; }
}