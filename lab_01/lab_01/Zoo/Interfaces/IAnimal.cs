namespace Zoo.Interfaces;

public interface IAnimal
{
    string Name { get; }
    string Description { get; }
    IType Type { get; }
}