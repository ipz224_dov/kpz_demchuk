namespace Zoo.Interfaces;

public interface IEmployee
{
    string Name { get; }
    int Age { get; }
    int Salary { get; set; }
}