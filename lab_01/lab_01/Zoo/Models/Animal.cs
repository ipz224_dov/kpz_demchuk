using Zoo.Interfaces;

namespace Zoo.Models;

public class Animal: IAnimal
{
    public string Name { get; }
    public string Description { get; }
    public IType Type { get; }
    
    public Animal(string name, string description, IType type)
    {
        Name = name;
        Description = description;
        Type = type;
    }
    
    public override string ToString()
    {
        return $"Name: {Name}, Description: {Description}, Type: {Type.Name}";
    }
}