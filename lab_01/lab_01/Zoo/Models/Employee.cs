using Zoo.Interfaces;

namespace Zoo.Models;

public class Employee: IEmployee
{
    public string Name { get; }
    public int Age { get; }
    public int Salary { get; set; }

    public Employee(string name, int age, int salary)
    {
        Name = name;
        Age = age;
        Salary = salary;
    }

    public override string ToString()
    {
        return $"Name: {Name}, Age: {Age}, Salary: {Salary}";
    }
}