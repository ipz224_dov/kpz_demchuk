using Zoo.Interfaces;

namespace Zoo.Models;

public class Type: IType
{
    public string Name { get; }
    public string Description { get; }
    
    public Type(string name, string description)
    {
        Name = name;
        Description = description;
    }
}