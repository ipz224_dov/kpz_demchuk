using Zoo.Interfaces;

namespace Zoo.Models.Inventory;

public class AnimalInventory: InventoryBase<IAnimal>
{
    public AnimalInventory(List<IAnimal> entities) : base(entities)
    {
    }
}