using Zoo.Interfaces;

namespace Zoo.Models.Inventory;

public class EmployeeInventory: InventoryBase<IEmployee>
{
    protected internal EmployeeInventory(List<IEmployee> entities) : base(entities)
    {
    }
}