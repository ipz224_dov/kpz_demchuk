using Zoo.Interfaces;

namespace Zoo.Models.Inventory;

public class CageInventory: InventoryBase<ICage>
{
    public CageInventory(List<ICage> entities) : base(entities)
    {
    }
}