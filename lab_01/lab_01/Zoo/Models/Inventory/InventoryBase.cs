using Zoo.Interfaces;

namespace Zoo.Models.Inventory;

public class InventoryBase<T> : IInventory<T>
{
    public List<T> Entities { get; set; }

    protected InventoryBase(List<T> entities)
    {
        Entities = entities;
    }
    public void DisplayInventory(List<T> entities)
    {
        Console.WriteLine($"All {typeof(T).Name}s:");
        foreach (var entity in Entities)
        {
            Console.WriteLine(entity.ToString());
        }
    }
}