using Zoo.Interfaces;
using Zoo.Validators.CageValidator;

namespace Zoo.Models;

public class Cage: ICage
{
    public IType AnimalType { get; }
    public List<IAnimal> Animals { get; }
    public int Capacity { get; }
    private readonly ICageValidator _cageValidator;
    
    public Cage(IType animalType, int capacity, ICageValidator cageValidator)
    {
        AnimalType = animalType;
        Capacity = capacity;
        Animals = new List<IAnimal>();
        _cageValidator = cageValidator;
    }
    public void AddAnimal(IAnimal animal)
    {
        _cageValidator.CanAddAnimal(this, animal);
        Animals.Add(animal);
    }

    public void RemoveAnimal(IAnimal animal)
    {
        _cageValidator.CanRemoveAnimal(this, animal);
        Animals.Remove(animal);
    }

    public override string ToString()
    {
        return $"{AnimalType.Name} with {Animals.Count} animals:\n {string.Join("    \n ", Animals)}";
    }
}