using Zoo.Interfaces;
using Zoo.Validators.EmployeeValidator;

namespace Zoo.Models;

public class Administrator: IAdministrator
{
    public string Name { get; }
    public int Age { get; }
    public int Salary { get; set; }
    public List<IEmployee> Employees { get; }
    private readonly IEmployeeValidator _employeeValidator;
    
    public Administrator(string name, int age, int salary, IEmployeeValidator employeeValidator)
    {
        Name = name;
        Age = age;
        Salary = salary;
        Employees = new List<IEmployee>();
        _employeeValidator = employeeValidator;
    }
    
    public void AddEmployee(IEmployee employee)
    {
        _employeeValidator.CanAddEmployee(employee, Employees);
        Employees.Add(employee);
    }

    public void RemoveEmployee(IEmployee employee)
    {
        _employeeValidator.CanRemoveEmployee(employee, Employees);
        Employees.Remove(employee);
    }

    public void ChangeSalary(IEmployee employee, int salary)
    {
        _employeeValidator.CanChangeSalary(employee, salary);
        employee.Salary = salary;
    }
}