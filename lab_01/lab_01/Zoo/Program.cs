﻿using Zoo.Interfaces;
using Zoo.Models;
using Zoo.Models.Inventory;
using Zoo.Validators.CageValidator;
using Zoo.Validators.EmployeeValidator;
using Type = Zoo.Models.Type;

namespace Zoo
{
    class Program
    {
        static void Main(string[] args)
        {
            CageValidator cageValidator = new CageValidator();
            EmployeeValidator employeeValidator = new EmployeeValidator();

            Employee employee1 = new Employee("Whytalik", 18, 3000);
            Employee employee2 = new Employee("Katya", 19, 3000);

            Administrator administrator = new Administrator("Olesia", 19, 40000, employeeValidator);
            administrator.AddEmployee(employee1);
            administrator.AddEmployee(employee2);
            Console.WriteLine("-----------------------------------------------------------------");
            EmployeeInventory employeeInventory = new EmployeeInventory(administrator.Employees);
            employeeInventory.DisplayInventory(employeeInventory.Entities);
            Console.WriteLine("-----------------------------------------------------------------\n");

            Type predator = new Type("Predator", "predator");
            Type herbivore = new Type("Herbivore", "herbivore");
            Type omnivorous = new Type("Omnivorous", "omnivorous");

            Animal wolf = new Animal("Wolf", "A big and angry wolf", predator);
            Animal lion = new Animal("Lion", "The king of the jungle", predator);

            Animal giraffe = new Animal("Giraffe", "Tall and herbivorous", herbivore);
            Animal elephant = new Animal("Elephant", "Large and herbivorous", herbivore);

            Animal bear = new Animal("Bear", "A versatile omnivore", omnivorous);
            Animal raccoon = new Animal("Raccoon", "Clever omnivore", omnivorous);

            Console.WriteLine("-----------------------------------------------------------------");
            AnimalInventory animalInventory = new AnimalInventory(new List<IAnimal> { wolf, lion, giraffe, elephant, bear, raccoon });
            animalInventory.DisplayInventory(animalInventory.Entities);
            Console.WriteLine("-----------------------------------------------------------------\n");

            Cage predatorCage = new Cage(predator, 2, cageValidator);
            predatorCage.AddAnimal(wolf);
            predatorCage.AddAnimal(lion);

            Cage herbivoreCage = new Cage(herbivore, 2, cageValidator);
            herbivoreCage.AddAnimal(giraffe);
            herbivoreCage.AddAnimal(elephant);

            Cage omnivorousCage = new Cage(omnivorous, 2, cageValidator);
            omnivorousCage.AddAnimal(bear);
            omnivorousCage.AddAnimal(raccoon);

            Console.WriteLine("-----------------------------------------------------------------");
            CageInventory cageInventory = new CageInventory(new List<ICage> { predatorCage, herbivoreCage, omnivorousCage});

        cageInventory.DisplayInventory(cageInventory.Entities);
            Console.WriteLine("-----------------------------------------------------------------");
        }
    }
}