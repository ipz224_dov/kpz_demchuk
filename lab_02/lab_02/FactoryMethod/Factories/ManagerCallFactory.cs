using FactoryMethod.Factories.Interfaces;
using FactoryMethod.Subscriptions;

namespace FactoryMethod.Factories;

public class ManagerCallFactory: IPurchase
{
    public Subscription CreateSubscription(string name)
    {
        Console.WriteLine("\n---------------------------------------");
        Console.WriteLine(" Buy subscription by Manager Call");
        return name switch
        {
            "D" => new DomesticSubscription("Domestic Subscription", 250,
                ["Domestic channel1", "Domestic channel2", "Domestic channel3"], 3),
        
            "E" => new EducationalSubscription("Educational Subscription", 450,
                [ "Educational channel1", "Educational channel2", "Educational channel3"], 6),
        
            "F" => new PremiumSubscription("Premium Subscription", 900,
                ["Premium channel1", "Premium channel2", "Premium channel3"], 12),
        
            _ => throw new ArgumentException("Invalid subscription type")
        };
    }

}