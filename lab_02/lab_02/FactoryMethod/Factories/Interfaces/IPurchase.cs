using FactoryMethod.Subscriptions;

namespace FactoryMethod.Factories.Interfaces;

public interface IPurchase
{
    public Subscription CreateSubscription(string name);
}