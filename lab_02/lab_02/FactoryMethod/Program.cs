﻿using FactoryMethod.Factories;
using FactoryMethod.Factories.Interfaces;

Console.WriteLine("Choose the way to purchase subscription:");
Console.WriteLine("A) Website");
Console.WriteLine("B) Mobile App");
Console.WriteLine("C) Manager Call");
Console.WriteLine("------------------------------------------------\n");
            

var option = Console.ReadLine()?.ToUpper();

IPurchase purchasePath;
switch (option)
{
    case "A":
        purchasePath = new WebSiteFactory();
        break;
    case "B":
        purchasePath = new MobileAppFactory();
        break;
    case "C":
        purchasePath = new ManagerCallFactory();
        break;
    default:
        Console.WriteLine("Invalid option");
        return;
}

Console.WriteLine("Choose the subscription type:");
Console.WriteLine("D) Domestic");
Console.WriteLine("E) Educational");
Console.WriteLine("F) Premium");
Console.WriteLine("------------------------------------------------\n");

var subscriptionType = Console.ReadLine()?.ToUpper();

try
{
    var subscription = purchasePath.CreateSubscription(subscriptionType!);
    subscription.GetSubscriptionInfo();
}
catch (ArgumentException ex)
{
    Console.WriteLine($"Error: {ex.Message}");
}