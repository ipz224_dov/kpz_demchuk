namespace FactoryMethod.Subscriptions;

public abstract class Subscription
{
    private readonly decimal _monthlyFee;
    private readonly int _minimumSubscriptionPeriod;

    private string Name { get; set; }

    private decimal MonthlyFee
    {
        get => _monthlyFee;
        init
        {
            if (value < 0)
                throw new ArgumentException("Monthly fee cannot be negative");
            _monthlyFee = value;
        }
    }

    private List<string> Channels { get; }

    private int MinimumSubscriptionPeriod
    {
        get => _minimumSubscriptionPeriod;
        init
        {
            if (value <= 0)
                throw new ArgumentException("Minimum subscription period must be a positive integer");
            _minimumSubscriptionPeriod = value;
        }
    }

    protected Subscription(string name, decimal monthlyFee, List<string> channels, int minimumSubscriptionPeriod)
    {
        Name = name;
        MonthlyFee = monthlyFee;
        Channels = channels;
        MinimumSubscriptionPeriod = minimumSubscriptionPeriod;
    }
    
    public void GetSubscriptionInfo()
    {
        Console.WriteLine($"Name: {Name}");
        Console.WriteLine($"Monthly fee: {MonthlyFee}");
        Console.WriteLine($"Chanel: {string.Join(", ", Channels)}");
        Console.WriteLine($"Minimum subscription period: {MinimumSubscriptionPeriod} months");
    }
}