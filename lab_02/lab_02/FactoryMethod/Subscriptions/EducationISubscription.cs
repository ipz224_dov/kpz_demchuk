namespace FactoryMethod.Subscriptions;

public class EducationalSubscription: Subscription
{
    public EducationalSubscription(string name, decimal monthlyFee, List<string> channels, int minimumSubscriptionPeriod) : base(name, monthlyFee, channels, minimumSubscriptionPeriod)
    {
    }
}