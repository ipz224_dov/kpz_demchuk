namespace FactoryMethod.Subscriptions;

public class DomesticSubscription: Subscription
{
    public DomesticSubscription(string name, decimal monthlyFee, List<string> channels, int minimumSubscriptionPeriod) : base(name, monthlyFee, channels, minimumSubscriptionPeriod)
    {
    }
}