namespace FactoryMethod.Subscriptions;

public class PremiumSubscription: Subscription
{
    public PremiumSubscription(string name, decimal monthlyFee, List<string> channels, int minimumSubscriptionPeriod) : base(name, monthlyFee, channels, minimumSubscriptionPeriod)
    {
    }
}