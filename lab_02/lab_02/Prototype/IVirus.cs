namespace Prototype;

public interface IVirus
{
    public void AddChild(Virus child);
    public IVirus Clone();
}