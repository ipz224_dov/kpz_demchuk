using System;
using System.Collections.Generic;

namespace Prototype
{
    public class Virus : ICloneable
    {
        private readonly double _weight;
        private readonly int _age;
        private readonly string _name;
        private readonly string _type;
        public readonly List<Virus> Children;

        public Virus(double weight, int age, string name, string type)
        {
            _weight = weight;
            _age = age;
            _name = name;
            _type = type;
            Children = new List<Virus>();
        }

        public void AddChild(Virus child)
        {
            Children.Add(child);
        }

        public object Clone()
        {
            var clone = new Virus(_weight, _age, _name, _type);
            foreach (var child in Children)
            {
                clone.AddChild((Virus)child.Clone());
            }
            return clone;
        }

        public override string ToString()
        {
            var result = $" Name = {_name}, Weight = {_weight}, Age = {_age}, Type = {_type}";
            
            if (Children.Count == 0) return result;
            result += "\n   Children = \n";
            result = Children.Aggregate(result, (current, child) => current + ("     + " + child + "\n"));

            return result;
        }
    }
}