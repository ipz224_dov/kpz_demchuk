﻿namespace Singleton;

class Program
{
    static void Main(string[] args)
    {
        // Create instances of Authenticator
        Authenticator authenticator1 = Authenticator.GetInstance();
        Authenticator authenticator2 = Authenticator.GetInstance();

        // Check if both references point to the same instance
        Console.WriteLine(authenticator1 == authenticator2); // Output: True
        
        InheritedAuthenticator inheritedAuthenticator = InheritedAuthenticator.GetInstance() as InheritedAuthenticator;
        InheritedAuthenticator inheritedAuthenticator2 = InheritedAuthenticator.GetInstance() as InheritedAuthenticator;

        // Check if both references point to the same instance
        Console.WriteLine(inheritedAuthenticator == inheritedAuthenticator2); // Output: True
    }
}