namespace Singleton
{
    public class Authenticator
    {
        private static Authenticator? _instance;
        private static readonly object Lock = new(); 

        protected Authenticator() { }

        public static Authenticator GetInstance()
        {
            lock (Lock)
            {
                if (_instance == null)
                {
                    _instance = new Authenticator();
                }

                return _instance;
            }
        }
    }
}