namespace Builder.Characters;

public class Character
{
    public string Name { get; set; }
    public string Gender { get; set; }
    public int Age { get; set; }
    public string BodyType { get; set; }
    public string HairColor { get; set; }
    public string EyeColor { get; set; }
    public string Weapon { get; set; }

    public override string ToString()
    {
        return $"Name - {Name}, Gender - {Gender}, Age - {Age}\n" +
               $"BodyType - {BodyType}, HairColor - {HairColor}\n" +
               $"EyeColor - {EyeColor}, Weapon - {Weapon}";
    }
}