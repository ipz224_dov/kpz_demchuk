﻿using Builder;
using Builder.Builders;

var hero = Director.BuildHero(new HeroBuilder());
var enemy = Director.BuildEnemy(new EnemyBuilder());

Console.WriteLine(hero);
Console.WriteLine(enemy);