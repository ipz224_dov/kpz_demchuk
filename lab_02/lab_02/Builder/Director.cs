using Builder.Builders;
using Builder.Characters;

namespace Builder;

public static class Director
{
    public static Character BuildHero(HeroBuilder builder)
    {
        builder
            .SetName("Hero")
            .SetGender("Male")
            .SetAge(20)
            .SetBodyType("Human")
            .SetHairColor("Black")
            .SetEyeColor("Blue")
            .SetWeapon("Sword");
        
        return builder
            .Build();
    }
    
    public static Character BuildEnemy(EnemyBuilder builder)
    {
        builder
            .SetName("Enemy")
            .SetGender("Male")
            .SetAge(20)
            .SetBodyType("Human")
            .SetHairColor("Black")
            .SetEyeColor("Blue")
            .SetWeapon("Sword");
        
        return builder
            .Build();
    }
}