using Builder.Characters;

namespace Builder.Builders;

public interface IBuilder
{
    IBuilder SetName(string name);
    IBuilder SetGender(string gender);
    IBuilder SetAge(int age);
    IBuilder SetBodyType(string bodyType);
    IBuilder SetHairColor(string color);
    IBuilder SetEyeColor(string color);
    IBuilder SetWeapon(string weapon);
    Character Build();
}