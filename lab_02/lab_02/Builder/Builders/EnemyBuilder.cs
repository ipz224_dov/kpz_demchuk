using Builder.Characters;

namespace Builder.Builders;

public class EnemyBuilder: IBuilder
{
    private readonly Character _enemy = new Character();
    public IBuilder SetName(string name)
    {
        _enemy.Name = name;
        return this;
    }

    public IBuilder SetGender(string gender)
    {
        _enemy.Gender = gender;
        return this;
    }

    public IBuilder SetAge(int age)
    {
        _enemy.Age = age;
        return this;
    }

    public IBuilder SetBodyType(string bodyType)
    {
        _enemy.BodyType = bodyType;
        return this;
    }

    public IBuilder SetHairColor(string color)
    {
        _enemy.HairColor = color;
        return this;
    }

    public IBuilder SetEyeColor(string color)
    {
        _enemy.EyeColor = color;
        return this;
    }
    
    public IBuilder SetWeapon(string weapon)
    {
        _enemy.Weapon = weapon;
        return this;
    }

    public Character Build()
    {
        return _enemy;
    }
}