using Builder.Characters;

namespace Builder.Builders;

public class HeroBuilder: IBuilder
{
    private readonly Character _hero = new Character();
    public IBuilder SetName(string name)
    {
        _hero.Name = name;
        return this;
    }

    public IBuilder SetGender(string gender)
    {
        _hero.Gender = gender;
        return this;
    }

    public IBuilder SetAge(int age)
    {
        _hero.Age = age;
        return this;
    }

    public IBuilder SetBodyType(string bodyType)
    {
        _hero.BodyType = bodyType;
        return this;
    }

    public IBuilder SetHairColor(string color)
    {
        _hero.HairColor = color;
        return this;
    }

    public IBuilder SetEyeColor(string color)
    {
        _hero.EyeColor = color;
        return this;
    }

    public IBuilder SetWeapon(string weapon)
    {
        _hero.Weapon = weapon;
        return this;
    }

    public Character Build()
    {
        return _hero;
    }
}