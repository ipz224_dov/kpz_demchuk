namespace AbstractFactory.Devices;

public interface IDevice
{
    public string GetDeviceNameAndFirm();
}