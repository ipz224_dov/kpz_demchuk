using AbstractFactory.Devices.Interfaces;

namespace AbstractFactory.Devices.Products.BalaxyProducts;

public class BalaxyNetbook: INetbook
{
    public string GetDeviceNameAndFirm()
    {
        return "Balaxy Netbook";
    }
}