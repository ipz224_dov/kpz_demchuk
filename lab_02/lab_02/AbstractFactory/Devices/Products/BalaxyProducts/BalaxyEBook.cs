using AbstractFactory.Devices.Interfaces;

namespace AbstractFactory.Devices.Products.BalaxyProducts;

public class BalaxyEBook: IEBook
{
    public string GetDeviceNameAndFirm()
    {
        return "Balaxy EBook";
    }
}