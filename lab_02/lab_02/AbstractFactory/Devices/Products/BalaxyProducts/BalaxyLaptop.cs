using AbstractFactory.Devices.Interfaces;

namespace AbstractFactory.Devices.Products.BalaxyProducts;

public class BalaxyLaptop: ILaptop
{
    public string GetDeviceNameAndFirm()
    {
        return "Balaxy Laptop";
    }
}