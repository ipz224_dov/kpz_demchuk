using AbstractFactory.Devices.Interfaces;

namespace AbstractFactory.Devices.Products.BalaxyProducts;

public class BalaxySmartphone: ISmartphone
{
    public string GetDeviceNameAndFirm()
    {
        return "Balaxy smartphone";
    }
}