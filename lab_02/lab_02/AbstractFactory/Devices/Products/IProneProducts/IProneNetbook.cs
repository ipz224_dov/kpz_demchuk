using AbstractFactory.Devices.Interfaces;

namespace AbstractFactory.Devices.Products.IProneProducts;

public class IProneNetbook: INetbook
{
    public string GetDeviceNameAndFirm()
    {
        return "IProne Netbook";
    }
}