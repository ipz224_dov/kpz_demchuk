using AbstractFactory.Devices.Interfaces;

namespace AbstractFactory.Devices.Products.IProneProducts;

public class IProneSmartphone: ISmartphone
{
    public string GetDeviceNameAndFirm()
    {
        return "IProne Smartphone";
    }
}