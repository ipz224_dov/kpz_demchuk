using AbstractFactory.Devices.Interfaces;

namespace AbstractFactory.Devices.Products.IProneProducts;

public class IProneLaptop: ILaptop
{
    public string GetDeviceNameAndFirm()
    {
        return "IProne laptop";
    }
}