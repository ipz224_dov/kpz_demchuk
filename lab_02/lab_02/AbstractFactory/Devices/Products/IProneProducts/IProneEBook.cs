using AbstractFactory.Devices.Interfaces;

namespace AbstractFactory.Devices.Products.IProneProducts;

public class IProneEBook: IEBook
{
    public string GetDeviceNameAndFirm()
    {
        return "IProne EBook";
    }
}