using AbstractFactory.Devices.Interfaces;

namespace AbstractFactory.Devices.Products.KiaomiProducts;

public class KiaomiSmartphone: ISmartphone
{
    public string GetDeviceNameAndFirm()
    {
        return "Kiaomi Smartphone";
    }
}