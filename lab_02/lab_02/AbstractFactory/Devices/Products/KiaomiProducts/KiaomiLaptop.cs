using AbstractFactory.Devices.Interfaces;

namespace AbstractFactory.Devices.Products.KiaomiProducts;

public class KiaomiLaptop: ILaptop
{
    public string GetDeviceNameAndFirm()
    {
        return "Kiaomi laptop";
    }
}