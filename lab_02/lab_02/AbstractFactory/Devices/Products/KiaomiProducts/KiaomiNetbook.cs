using AbstractFactory.Devices.Interfaces;

namespace AbstractFactory.Devices.Products.KiaomiProducts;

public class KiaomiNetbook: INetbook
{
    public string GetDeviceNameAndFirm()
    {
        return "Kiaomi Netbook";
    }
}