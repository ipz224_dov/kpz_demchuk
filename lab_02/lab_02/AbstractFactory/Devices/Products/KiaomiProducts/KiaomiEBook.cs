using AbstractFactory.Devices.Interfaces;

namespace AbstractFactory.Devices.Products.KiaomiProducts;

public class KiaomiEBook: IEBook
{
    public string GetDeviceNameAndFirm()
    {
        return "Kiaomi EBook";
    }
}