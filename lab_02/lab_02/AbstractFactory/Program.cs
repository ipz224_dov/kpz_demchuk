using AbstractFactory.Factories;

Console.WriteLine("\n > Balaxy Factory");
Console.WriteLine("------------------------------------");
ClientMethod(new BalaxyFactory());
        

Console.WriteLine("\n > IProne Factory");
Console.WriteLine("------------------------------------");
ClientMethod(new IProneFactory());
        
        
Console.WriteLine("\n > Kiaomi Factory");
Console.WriteLine("------------------------------------");
ClientMethod(new KiaomiFactory());

void ClientMethod(IFactory factory)
{
    var laptop = factory.CreateLaptop();
    var netbook = factory.CreateNetbook();
    var smartphone = factory.CreateSmartphone();
    var ebook = factory.CreateEBook();
            
    Console.WriteLine(">>> " + laptop.GetDeviceNameAndFirm());
    Console.WriteLine(">>> " + netbook.GetDeviceNameAndFirm());
    Console.WriteLine(">>> " + smartphone.GetDeviceNameAndFirm());
    Console.WriteLine(">>> " + ebook.GetDeviceNameAndFirm());
    Console.WriteLine("\n------------------------------------");
}