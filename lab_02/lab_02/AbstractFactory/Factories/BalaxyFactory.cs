using AbstractFactory.Devices.Interfaces;
using AbstractFactory.Devices.Products.BalaxyProducts;

namespace AbstractFactory.Factories;

public class BalaxyFactory: IFactory
{
    public IEBook CreateEBook()
    {
        return new BalaxyEBook();
    }

    public ILaptop CreateLaptop()
    {
        return new BalaxyLaptop();
    }

    public INetbook CreateNetbook()
    {
        return new BalaxyNetbook();
    }

    public ISmartphone CreateSmartphone()
    {
        return new BalaxySmartphone();
    }
}