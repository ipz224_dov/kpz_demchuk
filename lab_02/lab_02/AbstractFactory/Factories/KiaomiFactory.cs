using AbstractFactory.Devices.Interfaces;
using AbstractFactory.Devices.Products.KiaomiProducts;

namespace AbstractFactory.Factories;

public class KiaomiFactory: IFactory
{
    public IEBook CreateEBook()
    {
        return new KiaomiEBook();
    }

    public ILaptop CreateLaptop()
    {
        return new KiaomiLaptop();
    }

    public INetbook CreateNetbook()
    {
        return new KiaomiNetbook();
    }

    public ISmartphone CreateSmartphone()
    {
        return new KiaomiSmartphone();
    }
}