using AbstractFactory.Devices.Interfaces;

namespace AbstractFactory.Factories;

public interface IFactory
{
    public IEBook CreateEBook();
    public ILaptop CreateLaptop();
    public INetbook CreateNetbook();
    public ISmartphone CreateSmartphone();

}