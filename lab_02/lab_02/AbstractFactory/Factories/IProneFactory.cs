using AbstractFactory.Devices.Interfaces;
using AbstractFactory.Devices.Products.IProneProducts;

namespace AbstractFactory.Factories;

public class IProneFactory: IFactory
{
    public IEBook CreateEBook()
    {
        return new IProneEBook();
    }

    public ILaptop CreateLaptop()
    {
        return new IProneLaptop();
    }

    public INetbook CreateNetbook()
    {
        return new IProneNetbook();
    }

    public ISmartphone CreateSmartphone()
    {
        return new IProneSmartphone();
    }
}