﻿using Strategy;
using Strategy.LoadWays;

var networkLoadStrategy = new NetworkLoad(); 
var fileLoadStrategy = new FileLoad();

const string networkImage = "https://img.freepik.com/free-photo/fresh-yellow-daisy-single-flower-close-up-beauty-generated-by-ai_188544-15543.jpg";
const string fileImage = @"C:\Users\old23\OneDrive\Desktop\KPZ_Lab04_Demchuk\KPZ_Lab04_Demchuk\Strategy\image.jpg";

var networkLightImage = new LightImage(networkLoadStrategy, networkImage, "image", new List<string>() { "network", "image" });
var fileLightImage = new LightImage(fileLoadStrategy, fileImage, "image", new List<string>() { "file", "image" });

Console.WriteLine("Network image:");
Console.WriteLine(networkLightImage.OuterHtml);

Console.WriteLine("File image:");
Console.WriteLine(fileLightImage.OuterHtml);