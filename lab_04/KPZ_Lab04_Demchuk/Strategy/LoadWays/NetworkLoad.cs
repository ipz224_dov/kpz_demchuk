namespace Strategy.LoadWays;

public class NetworkLoad: IImageLoadStrategy
{
    public async Task<byte[]> LoadImageAsync(string href)
    {
        var httpClient = new HttpClient();
        var response = await httpClient.GetAsync(href);
        return await response.Content.ReadAsByteArrayAsync();
    }
}