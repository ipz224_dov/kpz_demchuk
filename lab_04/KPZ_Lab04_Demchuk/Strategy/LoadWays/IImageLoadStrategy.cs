namespace Strategy.LoadWays;

public interface IImageLoadStrategy
{
    Task<byte[]> LoadImageAsync(string href);
}