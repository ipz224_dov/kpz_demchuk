namespace Strategy.LoadWays;

public class FileLoad: IImageLoadStrategy
{
    public async Task<byte[]> LoadImageAsync(string href)
    {
        var bytes = await File.ReadAllBytesAsync(href);
        return bytes;
    }
}