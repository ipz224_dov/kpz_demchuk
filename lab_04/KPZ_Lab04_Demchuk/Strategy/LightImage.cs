using Observer.CompositeClasses;
using Strategy.LoadWays;

namespace Strategy
{
    public class LightImage : LightElementNode
    {
        public LightImage(IImageLoadStrategy imageLoadStrategy, string href, string alt, List<string>? cssClasses = null) : 
            base("img", false, true, cssClasses)
        {
            _imageLoadStrategy = imageLoadStrategy;
            AddAttribute("href", href);
            AddAttribute("alt", alt);
        }
        
        private readonly IImageLoadStrategy _imageLoadStrategy;

        public async Task<byte[]> LoadImageAsync(string href)
        {
            return await _imageLoadStrategy.LoadImageAsync(href);
        }
    }
}