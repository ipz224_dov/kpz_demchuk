namespace ChainOfResponsibility.Entities;

public class User(string name)
{
    public string Name { get; } = name;
}