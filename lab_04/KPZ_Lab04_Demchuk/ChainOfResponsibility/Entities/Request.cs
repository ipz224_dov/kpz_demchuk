using ChainOfResponsibility.Handlers.Levels;

namespace ChainOfResponsibility.Entities;

public class Request(User user, RequestTypes type)
{
    public User User { get; } = user;
    public RequestTypes Type { get; } = type;
}