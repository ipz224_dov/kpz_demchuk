﻿using ChainOfResponsibility.Entities;
using ChainOfResponsibility.Handlers.Levels;

User user = new User("Olesia");
Request request1 = new Request(user, RequestTypes.Test);
Request request2 = new Request(user, RequestTypes.Appointment);

AppointmentHandler appointmentHandler = new AppointmentHandler();
MedicalConsultationHandler medicalConsultationHandler = new MedicalConsultationHandler();
MedicalRecordHandler medicalRecordHandler = new MedicalRecordHandler();
MedicalTestHandler medicalTestHandler = new MedicalTestHandler();

appointmentHandler
    .SetNext(medicalConsultationHandler)
    .SetNext(medicalRecordHandler)
    .SetNext(medicalTestHandler);
    
appointmentHandler.Handle(request1);
Console.WriteLine("---------------------------------------------------------\n");
appointmentHandler.Handle(request2);