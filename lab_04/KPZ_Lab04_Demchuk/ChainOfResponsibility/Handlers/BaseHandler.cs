using ChainOfResponsibility.Entities;

namespace ChainOfResponsibility.Handlers;

public abstract class BaseHandler: IHandler
{
    private IHandler? _nextHandler;
    public IHandler SetNext(IHandler handler)
    {
        _nextHandler = handler;
        return handler;
    }

    public virtual void Handle(Request request)
    {
        if (_nextHandler != null)
        {
            _nextHandler.Handle(request);
        }
        else
        {
            Console.WriteLine("End of chain, no handler for {0}", request);
        }
    }
}