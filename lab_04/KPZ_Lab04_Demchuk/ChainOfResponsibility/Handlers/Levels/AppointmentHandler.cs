using ChainOfResponsibility.Entities;

namespace ChainOfResponsibility.Handlers.Levels;

public class AppointmentHandler: BaseHandler
{
    public override void Handle(Request request)
    {
        Console.WriteLine("First level handler");
        Console.WriteLine();
        if (request.Type == RequestTypes.Appointment)
        {
            Console.BackgroundColor = ConsoleColor.Red;
            Console.WriteLine("Appointment request handled for " + request.User.Name);
            Console.ResetColor();
        }
        else
        {
            base.Handle(request);
        }
    }
}