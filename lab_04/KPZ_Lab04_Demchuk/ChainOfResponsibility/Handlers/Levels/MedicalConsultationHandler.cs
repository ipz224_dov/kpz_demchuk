using ChainOfResponsibility.Entities;

namespace ChainOfResponsibility.Handlers.Levels;

public class MedicalConsultationHandler: BaseHandler
{
    public override void Handle(Request request)
    {
        Console.WriteLine("Second level handler");
        Console.WriteLine();
        if (request.Type == RequestTypes.Consultation)
        {
            Console.BackgroundColor = ConsoleColor.Red;
            Console.WriteLine("Consultation request handled for " + request.User.Name);
            Console.ResetColor();
        }
        else
        {
            base.Handle(request);
        }
    }
}