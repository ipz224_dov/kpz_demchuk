using ChainOfResponsibility.Entities;

namespace ChainOfResponsibility.Handlers.Levels;

public class MedicalRecordHandler: BaseHandler
{
    public override void Handle(Request request)
    {
        Console.WriteLine("Third level handler");
        Console.WriteLine();
        if (request.Type == RequestTypes.Record)
        {
            Console.BackgroundColor = ConsoleColor.Red;
            Console.WriteLine("MedicalRecord request handled for " + request.User.Name);
            Console.ResetColor();
        }
        else
        {
            base.Handle(request);
        }
    }
}