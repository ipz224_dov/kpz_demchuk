using ChainOfResponsibility.Entities;

namespace ChainOfResponsibility.Handlers.Levels;

public class MedicalTestHandler: BaseHandler
{
    public override void Handle(Request request)
    {
        Console.WriteLine("Fourth level handler");
        Console.WriteLine();
        if (request.Type == RequestTypes.Test)
        {
            Console.BackgroundColor = ConsoleColor.Red;
            Console.WriteLine("MedicalTest request handled for " + request.User.Name);
            Console.ResetColor();
        }
        else
        {
            base.Handle(request);
        }
    }
}