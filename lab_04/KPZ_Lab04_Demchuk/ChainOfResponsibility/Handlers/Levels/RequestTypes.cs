namespace ChainOfResponsibility.Handlers.Levels;

public enum RequestTypes
{
    Appointment,
    Consultation,
    Record,
    Test
}