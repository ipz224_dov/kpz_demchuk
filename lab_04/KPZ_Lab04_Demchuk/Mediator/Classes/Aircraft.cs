using Mediator.Classes.Interfaces;

namespace Mediator.Classes;
public class Aircraft(string name): IAircraft
{
    public string Name { get; } = name;
    public Runway? CurrentRunway { get; set; }
    public bool IsTakingOff { get; private set; }


    public void Land()
    {
        IsTakingOff = false;
        Console.WriteLine($"Aircraft {Name} has landed.");
    }

    public void TakeOff()
    {
        IsTakingOff = true;
        Console.WriteLine($"Aircraft {Name} has taken off.");
    }
}