using Mediator.Classes.Interfaces;

namespace Mediator.Classes
{
    public class Runway: IRunway
    {
        public Guid Id { get; set; } = Guid.NewGuid();
        public Aircraft? IsBusyWithAircraft { get; set; } = null;

        public bool IsAvailable()
        {
            return IsBusyWithAircraft == null;
        }
        

        public void HighLightGreen()
        {
            Console.WriteLine($"Runway {Id} is free!");
        }
    }
}