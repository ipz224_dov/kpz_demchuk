namespace Mediator.Classes.Interfaces;

public interface IMediator
{
    void AircraftRequestsRunway(Aircraft aircraft);
    void AircraftRequestsTakeOff(Aircraft aircraft);
}