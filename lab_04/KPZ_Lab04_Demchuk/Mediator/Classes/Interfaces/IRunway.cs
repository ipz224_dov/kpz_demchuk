namespace Mediator.Classes.Interfaces;

public interface IRunway
{
    Guid Id { get; }
    public Aircraft? IsBusyWithAircraft { get; set; } 
    bool IsAvailable();
    void HighLightGreen();
}