using Mediator.Classes.Interfaces;

namespace Mediator.Classes;

public class CommandCentre : IMediator
{
    private readonly List<IRunway> _runways = new();
    private readonly List<IAircraft> _aircrafts = new();

    public CommandCentre(IRunway[] runways, IAircraft[] aircrafts)
    {
        _runways.AddRange(runways);
        _aircrafts.AddRange(aircrafts);
    }
    
    public void AircraftRequestsRunway(Aircraft aircraft)
    {
        
        Console.WriteLine("\n----------------------------------------------------------------------------------");
        Console.WriteLine($"Aircraft {aircraft.Name} requests a runway.");

        if (_runways.Any(runway => runway.IsBusyWithAircraft == aircraft))
        {
            Console.WriteLine($"Aircraft {aircraft.Name} has already requested a runway.");
            return;
        }

        foreach (var runway in _runways.Where(runway => runway.IsAvailable()))
        {
            Console.WriteLine($"Runway {runway.Id} is available. Aircraft {aircraft.Name} requests it.");
            runway.IsBusyWithAircraft = aircraft;
            aircraft.Land();
            return;
        }

        Console.WriteLine($"All runways are busy. Aircraft {aircraft.Name} waits.");
        Console.WriteLine("----------------------------------------------------------------------------------\n");
    }

    public void AircraftRequestsTakeOff(Aircraft aircraft)
    {
        Console.WriteLine("\n----------------------------------------------------------------------------------");
        Console.WriteLine($"Aircraft {aircraft.Name} requests take off.");

        if (aircraft.IsTakingOff)
        {
            Console.WriteLine($"Aircraft {aircraft.Name} has already requested take off.");
            return;
        }

        var currentRunway = _runways.FirstOrDefault(runway => runway.IsBusyWithAircraft == aircraft);
        if (currentRunway != null)
        {
            Console.WriteLine($"Aircraft {aircraft.Name} is taking off from runway {currentRunway.Id}");
            currentRunway.IsBusyWithAircraft = null;
            aircraft.TakeOff();
            currentRunway.HighLightGreen();
        }
        else
        {
            Console.WriteLine($"Aircraft {aircraft.Name} does not have a runway.");
        }
        Console.WriteLine("----------------------------------------------------------------------------------\n");
    }
}