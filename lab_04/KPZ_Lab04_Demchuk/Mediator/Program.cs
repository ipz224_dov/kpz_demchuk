﻿using Mediator.Classes;
using Mediator.Classes.Interfaces;

namespace Mediator
{
    class Program
    {
        static void Main(string[] args)
        {
            var runway1 = new Runway();
            var runway2 = new Runway();
            IRunway[] runways = [runway1, runway2];

            var aircraft1 = new Aircraft("Boeing 747");
            var aircraft2 = new Aircraft("Airbus A320");
            var aircraft3 = new Aircraft("Boeing 737");
            IAircraft[] aircrafts = [aircraft1, aircraft2, aircraft3];

            var commandCentre = new CommandCentre(runways, aircrafts);

            commandCentre.AircraftRequestsRunway(aircraft1);
            commandCentre.AircraftRequestsRunway(aircraft2);
            commandCentre.AircraftRequestsRunway(aircraft3);

            commandCentre.AircraftRequestsTakeOff(aircraft1);
            commandCentre.AircraftRequestsTakeOff(aircraft2);
            commandCentre.AircraftRequestsTakeOff(aircraft3);

            commandCentre.AircraftRequestsRunway(aircraft3);

            Console.ReadLine();
        }
    }
}