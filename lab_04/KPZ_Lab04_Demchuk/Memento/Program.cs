﻿using Memento;

var document = new TextDocument("MyDocument.txt", new List<string>());
var changesManager = new ChangesManager(document);

Console.WriteLine("Adding some text to the document:");
document.WriteLine("First line");
changesManager.Save(document.CreateMemento());
Console.WriteLine("Document after adding the first line:");
Console.WriteLine(document);

Console.WriteLine("\nAdding some more text to the document:");
document.WriteLine("Second line");
changesManager.Save(document.CreateMemento());
Console.WriteLine("Document after adding the second line:");
Console.WriteLine(document);

Console.WriteLine("\nAdding even more text to the document:");
document.WriteLine("Third line");
changesManager.Save(document.CreateMemento());
Console.WriteLine("Document after adding the third line:");
Console.WriteLine(document);

Console.WriteLine("\nUndoing the last change:");
changesManager.UndoChange();
Console.WriteLine("Document after undoing:");
Console.WriteLine(document);

Console.WriteLine("\nUndoing the change before that:");
changesManager.UndoChange();
Console.WriteLine("Document after undoing again:");
Console.WriteLine(document);

Console.WriteLine("\nAdding a new line:");
document.WriteLine("Fourth line");
changesManager.Save(document.CreateMemento());
Console.WriteLine("Document after adding the fourth line:");
Console.WriteLine(document);

Console.WriteLine("\nRedoing the last undone change:");
changesManager.RedoChange();
Console.WriteLine("Document after redoing:");
Console.WriteLine(document);

Console.WriteLine("\nRedoing the change before that:");
changesManager.RedoChange();
Console.WriteLine("Document after redoing again:");
Console.WriteLine(document);

Console.WriteLine("\nAdding more lines:");
document.WriteLine("Fifth line");
document.WriteLine("Sixth line");
document.WriteLine("Seventh line");
changesManager.Save(document.CreateMemento());
Console.WriteLine("Document after adding more lines:");
Console.WriteLine(document);

Console.WriteLine("\nUndoing multiple changes:");
changesManager.UndoChange();
changesManager.UndoChange();
Console.WriteLine("Document after undoing multiple changes:");
Console.WriteLine(document);
