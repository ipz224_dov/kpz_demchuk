namespace Memento;

public interface IMemento
{
    string Title { get; set; }
    List<string> Content { get; set; }
}