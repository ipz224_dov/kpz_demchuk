namespace Memento;

public class Memento(string title, List<string> content) : IMemento
{
    public string Title { get; set; } = title;
    public List<string> Content { get; set; } = [..content];
}