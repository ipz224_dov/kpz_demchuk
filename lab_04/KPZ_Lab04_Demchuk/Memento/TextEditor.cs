namespace Memento
{
    public class ChangesManager
    {
        private readonly List<IMemento> _changes = new();
        private int _current;
        private readonly TextDocument _document;

        public ChangesManager(TextDocument document)
        {
            _document = document;
            Save(_document.CreateMemento());
        }

        public void Save(IMemento memento)
        {
            _changes.Add(memento);
            _current = _changes.Count - 1;
        }

        public void ApplyMemento(IMemento memento)
        {
            _document.RestoreDocument(memento);
        }

        public TextDocument UndoChange()
        {
            if (_current > 0)
            {
                _current--;
                ApplyMemento(_changes[_current]);
            }
            return _document;
        }

        public TextDocument RedoChange()
        {
            if (_current + 1 < _changes.Count)
            {
                _current++;
                ApplyMemento(_changes[_current]);
            }
            return _document;
        }

        public override string ToString()
        {
            return _document.ToString();
        }
    }
}