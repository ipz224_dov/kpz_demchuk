namespace Memento;

public class TextDocument(string title, List<string> content)
{
    public string Title { get; set; } = title;
    public List<string> Content { get; set; } = [..content];

    public void WriteLine(string text)
    {
        Content.Add(text);
    }

    public IMemento CreateMemento()
    {
        return new Memento(Title, Content);
    }

    public void RestoreDocument(IMemento? memento)
    {
        if (memento == null) return;
        Title = memento.Title;
        Content = [..memento.Content];
    }
    
    public override string ToString()
    {
        return Title + Environment.NewLine + string.Join(Environment.NewLine, Content);
    }
}