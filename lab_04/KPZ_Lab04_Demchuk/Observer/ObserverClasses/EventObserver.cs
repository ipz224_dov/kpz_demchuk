using Observer.CompositeClasses;

namespace Observer.ObserverClasses
{
    public class EventObserver(string eventName) : IObserver
    {
        public void Update(string eventType, LightNode lightNode)
        {
            if (eventType == eventName)
            {
                Console.WriteLine($"EventObserver received event: {eventName}");
                Console.WriteLine($"Element affected:\n {lightNode.OuterHtml}");
                Console.WriteLine();
            }
        }
    }
}