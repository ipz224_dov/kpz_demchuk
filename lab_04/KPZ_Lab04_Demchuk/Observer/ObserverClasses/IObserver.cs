using Observer.CompositeClasses;

namespace Observer.ObserverClasses;

public interface IObserver
{
    void Update(string eventType, LightNode lightNode);
}