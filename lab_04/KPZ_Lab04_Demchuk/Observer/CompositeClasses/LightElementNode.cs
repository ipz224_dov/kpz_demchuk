using System.Collections.Generic;
using System.Text;
using Observer.ObserverClasses;

namespace Observer.CompositeClasses
{
    public class LightElementNode : LightNode, ISubject
    {
        private readonly string _tagName;
        public bool IsBlockLevel { get; }
        public bool isSelfClosing { get; }
        private readonly Dictionary<string, string> _attributes;
        private readonly List<string> _cssClasses;
        public List<LightNode> Children = new List<LightNode>();
        private readonly List<IObserver> _observers = new List<IObserver>();

        public LightElementNode(string tagName, bool isBlockLevel, bool isSelfClosing, List<string>? cssClasses = null, 
            Dictionary<string, string>? attributes = null)
        {
            _tagName = tagName;
            IsBlockLevel = isBlockLevel;
            _cssClasses = cssClasses ?? new List<string>();
            _attributes = attributes ?? new Dictionary<string, string>();
        }

        public void Attach(IObserver observer)
        {
            _observers.Add(observer);
        }

        public void Detach(IObserver observer)
        {
            _observers.Remove(observer);
        }

        public void Notify(string eventType)
        {
            foreach (var observer in _observers)
            {
                observer.Update(eventType, this);
            }
        }

        public void AddAttribute(string attributeName, string attributeValue)
        {
            _attributes.Add(attributeName, attributeValue);
        }

        public override string InnerHtml
        {
            get
            {
                var sb = new StringBuilder();

                foreach (var child in Children)
                {
                    sb.Append(child.OuterHtml);
                }

                return sb.ToString();
            }
        }

        public override string OuterHtml
        {
            get
            {
                var sb = new StringBuilder();
                sb.Append($"<{_tagName}");

                if (_attributes.Count > 0)
                {
                    foreach (var (attributeName, attributeValue) in _attributes)
                    {
                        sb.Append($" {attributeName}=\"{attributeValue}\"");
                    }
                }

                if (_cssClasses.Count > 0) ;
                {
                    sb.Append($" class=\"");
                    foreach (var cssClass in _cssClasses)
                    {
                        sb.Append($" {cssClass}");
                    }
                    
                    sb.Append("\"");
                }

                sb.Append(">");

                sb.Append(InnerHtml);

                if (isSelfClosing)
                {
                    sb.Append("/>");
                }
                else
                {
                    sb.Append($"</{_tagName}>");
                    sb.AppendLine();
                }

                return sb.ToString();
            }
        }
    }
}
