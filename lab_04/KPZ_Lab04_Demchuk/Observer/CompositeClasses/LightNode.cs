namespace Observer.CompositeClasses;

public abstract class LightNode
{
    public abstract string InnerHtml { get; }
    public abstract string OuterHtml { get; }
}