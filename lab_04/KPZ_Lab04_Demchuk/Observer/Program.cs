﻿using Observer.CompositeClasses;
using Observer.ObserverClasses;

IObserver clickObserver = new EventObserver("click");
IObserver mouseOverObserver = new EventObserver("mouseover");


var divElement = new LightElementNode("div", true, false, ["container"]);
var spanElement = new LightElementNode("span", false, false, ["highlight"]);


divElement.Attach(clickObserver);
spanElement.Attach(mouseOverObserver);

divElement.Children.Add(spanElement);

divElement.Notify("click");
spanElement.Notify("mouseover");